package com.saranya.week8;

public class RectangleClass {
    private String name;
    private double width;
    private double height;

    public RectangleClass(String name,double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }
    public String getName(){
        return name;
    }

    public double AreaRectangle() {
        double areas = width * height;
        return areas;
    }

    public double PerimeterRectangle() {
        double perimeter = 2 * (width + height);
        return perimeter;
    }
    public void print(){
        System.out.println(name + "Area: " + AreaRectangle());
        System.out.println(name + "Perimeter: " + PerimeterRectangle());
    }
}
