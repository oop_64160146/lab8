package com.saranya.week8;

public class TriangleClass {
    private String name;
    private double sideA;
    private double sideB;
    private double sideC;

    public TriangleClass(String name, double sideA, double sideB, double sideC) {
        this.name = name;
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public String getName() {
        return name;
    }

    public double AreaTriangle() {
        double S = (sideA + sideB + sideC) / 2;
        double tanS = (S - sideA) * (S - sideB) * (S - sideC);
        double areas = Math.sqrt(S * tanS);
        return areas;

    }

    public double PerimeterTriangle() {
        double perimeter = sideA + sideB + sideC;
        return perimeter;
    }

    public void print() {
        System.out.println(name + "Area: " + AreaTriangle());
        System.out.println(name + "Perimeter: " + PerimeterTriangle());
    }
}