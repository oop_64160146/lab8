package com.saranya.week8;

public class CircleClass {
    private String name;
    private double radius;

    public CircleClass(String name, double radius) {
        this.name = name;
        this.radius = radius;
    }

    public String getName() {
        return name;
    }

    public double AreaCircle() {
        double areas = Math.PI * (radius * radius);
        return areas;
    }

    public double PerimeterCiecle() {
        double perimeter = 2 * Math.PI * radius;
        return perimeter;
    }

    public void print() {
        System.out.println(name + "Area: " + AreaCircle());
        System.out.println(name + "Perimeter: " + PerimeterCiecle());
    }
}