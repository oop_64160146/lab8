package com.saranya.week8;

public class TestShape {
    public static void main(String[] args) {
        RectangleClass rect1 = new RectangleClass("rect1", 10, 5);
        rect1.AreaRectangle();
        rect1.PerimeterRectangle();
        rect1.print();

        RectangleClass rect2 = new RectangleClass("rect2", 5, 3);
        rect2.AreaRectangle();
        rect2.PerimeterRectangle();
        rect2.print();

        CircleClass circle1 = new CircleClass("circle1", 1);
        circle1.AreaCircle();
        circle1.PerimeterCiecle();
        circle1.print();

        CircleClass circle2 = new CircleClass("circle2", 2);
        circle2.AreaCircle();
        circle2.PerimeterCiecle();
        circle2.print();

        TriangleClass triangle1 = new TriangleClass("triangle1", 5, 5, 6);
        triangle1.AreaTriangle();
        triangle1.PerimeterTriangle();
        triangle1.print();
    }

}